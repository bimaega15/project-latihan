<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trx_pendaftaran', function (Blueprint $table) {
            $table->increments('id');
            $table->string('pendaftaran_number', 100);
            $table->dateTime('pendaftaran_wakturegis')->default(now());
            $table->boolean('pendaftaran_status')->default(false);
            $table->integer('master_petugas_id')->unsigned();
            $table->integer('master_jenis_pembayaran_id')->unsigned();
            $table->integer('master_pasien_id')->unsigned();
            $table->integer('master_layanan_id')->unsigned();

            $table->foreign('master_petugas_id')->references('id')->on('master_petugas')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('master_jenis_pembayaran_id')->references('id')->on('master_jenis_pembayaran')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('master_pasien_id')->references('id')->on('master_pasien')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('master_layanan_id')->references('id')->on('master_layanan')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trx_pendaftaran');
    }
};
