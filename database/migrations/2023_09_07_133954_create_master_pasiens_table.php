<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_pasien', function (Blueprint $table) {
            $table->increments('id');
            $table->string('pasien_rekammedis', 30);
            $table->string('pasien_nama', 200);
            $table->enum('pasien_jeniskelamin', ['L', 'P']);
            $table->date('pasien_tanggallahir');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_pasien');
    }
};
