<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trx_pelayanan', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('pelayanan_waktumulai');
            $table->dateTime('pelayanan_waktuselesai');
            $table->integer('trx_pendaftaran_id')->unsigned();
            $table->timestamps();

            $table->foreign('trx_pendaftaran_id')->on('trx_pendaftaran')->references('id')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trx_pelayanan');
    }
};
