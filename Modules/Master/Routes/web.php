<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('master')->group(function () {
    Route::get('/', 'MasterController@index');

    Route::prefix('jenisPembayaran')->group(function () {
        Route::get('/', 'MasterJenisPembayaranController@index');
    });

    Route::prefix('jenisRegis')->group(function () {
        Route::get('/', 'MasterJenisRegisController@index');
    });

    Route::prefix('layanan')->group(function () {
        Route::get('/', 'MasterLayananController@index');
    });

    Route::prefix('pasien')->group(function () {
        Route::get('/', 'MasterPasienController@index');
    });

    Route::prefix('petugas')->group(function () {
        Route::get('/', 'MasterPetugasController@index');
    });
});
